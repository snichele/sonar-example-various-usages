package fr.snichele.formations.sonar.superbbutfraudstatistics;

import org.junit.Test;

public class SomeBeanTest {
    
    @Test
    public void invoke_main_to_please_sonar() {
        SomeBean.main(new String[]{});
        SomeBean.main(new String[]{"3"});
        SomeBean.SOMEBEAN.unused();
    }
}
