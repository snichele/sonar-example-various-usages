package fr.snichele.formations.sonar.superbbutfraudstatistics;

import org.junit.Test;

/**
 *
 * @author snichele
 */
public class FraudingBeanTest {
    
    @Test
    public void just_calling_getters_and_setters_to_please_sonar() {
        FraudingBean fb = new FraudingBean();
        fb.setAaadzzccza(25);
        fb.setAaqsdzccza(25d);
        fb.setAzesd("2zed5d");
        fb.getAaadzzccza();
        fb.getAaqsdzccza();
        fb.getAzesd();
    }
    @Test
    public void lets_call_the_complex_method() {
        FraudingBean fb = new FraudingBean();
        fb.edjamafacashun(1, 1, "toto");
        fb.edjamafacashun(2, 1, "toto");
        fb.edjamafacashun(3, 1, "toto");
        fb.edjamafacashun(3, 1, "titi");
        fb.edjamafacashun(3, -1, "titi");
    }
}
