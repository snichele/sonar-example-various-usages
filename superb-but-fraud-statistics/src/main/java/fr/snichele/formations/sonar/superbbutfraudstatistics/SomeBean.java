/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.snichele.formations.sonar.superbbutfraudstatistics;

/**
 * Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent rhoncus pulvinar metus vel gravida. Ut aliquet ante
 * est. Nullam id libero odio, eget porta tortor. Suspendisse id sapien nisi. Fusce sem tellus, sodales nec mollis
 * viverra, sagittis vitae dui. Nulla in leo nisl, at tristique erat. Suspendisse rhoncus fringilla metus vestibulum
 * lobortis. Integer sed leo ut diam sollicitudin pharetra. Maecenas elementum placerat mi ut lobortis. Integer vitae
 * augue in purus porttitor imperdiet. * Nunc id est eget purus mattis feugiat. Praesent egestas turpis vel mi suscipit
 * cursus. Curabitur sit amet tempus odio. Morbi vulputate, libero quis eleifend aliquam, metus velit elementum nibh,
 * nec blandit urna ipsum a massa. Vivamus malesuada lorem at velit faucibus fringilla. Vestibulum rhoncus sapien sit
 * amet felis interdum sed pharetra elit condimentum. Sed ut eleifend leo. Duis volutpat tellus quis nibh blandit
 * suscipit. Donec consequat nunc luctus orci accumsan semper. Phasellus ac pretium arcu. * Curabitur sodales luctus
 * metus, vel ultricies purus gravida vitae. Aliquam sit amet justo nec ipsum porttitor sagittis aliquet at mauris.
 * Phasellus pulvinar mi et felis egestas condimentum. Nullam et posuere felis. Donec lacus erat, euismod a malesuada
 * nec, accumsan vel felis. Nulla facilisi. Ut quis lorem quis sapien porta eleifend vel sit amet dui. Pellentesque
 * volutpat est eget justo placerat vitae dapibus arcu elementum. Pellentesque enim velit, volutpat vel tempus a,
 * faucibus eu dui. Sed auctor, augue in cursus egestas, dui massa viverra ante, non tempus tortor nisi sed nunc. Fusce
 * risus risus, hendrerit eget scelerisque non, varius vitae lectus. Duis augue dui, volutpat eu imperdiet sit amet,
 * tempor eget magna. Integer malesuada sem non libero vehicula vel pharetra dolor ornare. Maecenas purus leo, egestas
 * in vulputate ut, pharetra ac diam. Aenean ornare, ipsum vitae sagittis porta, mauris orci consectetur tortor, non
 * viverra elit orci nec est.
 * est. Nullam id libero odio, eget porta tortor. Suspendisse id sapien nisi. Fusce sem tellus, sodales nec mollis
 * viverra, sagittis vitae dui. Nulla in leo nisl, at tristique erat. Suspendisse rhoncus fringilla metus vestibulum
 * lobortis. Integer sed leo ut diam sollicitudin pharetra. Maecenas elementum placerat mi ut lobortis. Integer vitae
 * augue in purus porttitor imperdiet. * Nunc id est eget purus mattis feugiat. Praesent egestas turpis vel mi suscipit
 * cursus. Curabitur sit amet tempus odio. Morbi vulputate, libero quis eleifend aliquam, metus velit elementum nibh,
 * nec blandit urna ipsum a massa. Vivamus malesuada lorem at velit faucibus fringilla. Vestibulum rhoncus sapien sit
 * amet felis interdum sed pharetra elit condimentum. Sed ut eleifend leo. Duis volutpat tellus quis nibh blandit
 * suscipit. Donec consequat nunc luctus orci accumsan semper. Phasellus ac pretium arcu. * Curabitur sodales luctus
 * metus, vel ultricies purus gravida vitae. Aliquam sit amet justo nec ipsum porttitor sagittis aliquet at mauris.
 * Phasellus pulvinar mi et felis egestas condimentum. Nullam et posuere felis. Donec lacus erat, euismod a malesuada
 * nec, accumsan vel felis. Nulla facilisi. Ut quis lorem quis sapien porta eleifend vel sit amet dui. Pellentesque
 * volutpat est eget justo placerat vitae dapibus arcu elementum. Pellentesque enim velit, volutpat vel tempus a,
 * faucibus eu dui. Sed auctor, augue in cursus egestas, dui massa viverra ante, non tempus tortor nisi sed nunc. Fusce
 * risus risus, hendrerit eget scelerisque non, varius vitae lectus. Duis augue dui, volutpat eu imperdiet sit amet,
 * tempor eget magna. Integer malesuada sem non libero vehicula vel pharetra dolor ornare. Maecenas purus leo, egestas
 * in vulputate ut, pharetra ac diam. Aenean ornare, ipsum vitae sagittis porta, mauris orci consectetur tortor, non
 * viverra elit orci nec est.
 *
 * @author snichele
 */
public final class SomeBean {

    /** A Const. for zeo */
    private static final int ZERO = 0;
    /** Number of parts */
    private static final int SIX = 5;
    /** Added for fixing bug of nullpoinetr */
    private static final int MORE = 2;
    /** Singleton */
    public static final SomeBean SOMEBEAN = new SomeBean();

    /** This is only a method added to remove a warning about instanatiating class with only static method ! */
    public void unused() {
    }

    /**
     * Hide
     */
    private SomeBean() {
    }

    /**
     * This is main
     *
     * @param args wathever you want
     */
    public static void main(String[] args) {
        FraudingBean fb = new FraudingBean();
        if (args.length > ZERO) {
            fb.edjamafacashun(SIX, MORE, args[ZERO]);
        } else {
            fb.edjamafacashun(SIX, MORE, "toto");
        }
    }
}
